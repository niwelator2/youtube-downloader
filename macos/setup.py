from setuptools import setup

setup(
    name="Youtube-DOWNLOADER",
    version="2.0",
    scripts=["../main.py"],
    icon=["../logo.ico"],
    maintainer="niwelator2",
    maintainer_email="niwelator2@gmail.com",
    description="A simple Python script to download YouTube videos",
)
